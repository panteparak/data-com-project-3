import Discovery.*;
import Tracking.ServerMain;
import Transmission.Client.TorrentClient;
import Transmission.Server.TorrentTracker;
import org.eclipse.jetty.server.Server;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URLDecoder;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args) throws IOException {
        ArrayList<String> argument = new ArrayList<>(Arrays.asList(args));

        String url = Main.class.getProtectionDomain().getCodeSource().getLocation().getFile();
        String decode = URLDecoder.decode(url, "UTF-8");
        String SaveFolder = new File(decode).getParentFile().getPath();
        Scanner in = new Scanner(System.in);
        int n;
        InetAddress intrfc = null;

        if (((argument.contains("-transServer") && argument.contains("-i")))){
            ArrayList<InetAddress> ifaces = new ArrayList<>(TorrentClient.getInetInterface());
            if (ifaces.size() == 0) {
                System.out.println("Can't detect interfaces"); return;}
            for (InetAddress iface: ifaces) {
                n = ifaces.indexOf(iface);
                System.out.println(n + ")\t" + iface.getCanonicalHostName());
            }
            boolean gotInterface = false;
            do {
                n = in.nextInt();
                if (n < ifaces.size()) gotInterface = true;
                else System.out.println("invalid entry");
            }  while (!gotInterface);
            intrfc = ifaces.get(n);

            int input = argument.indexOf("-i")+1;

            TorrentTracker.createTorrentFile(Paths.get(SaveFolder, argument.get(input)).toString(), Paths.get(SaveFolder, "torrent.torrent").toString(), intrfc);
            new MulticastThread(intrfc).start();
            new ConnectionThread().start();

            ServerMain.startWebServer(Paths.get(SaveFolder, "TorrentFile").toFile());

            System.out.println(Paths.get(SaveFolder, argument.get(input)).toString());
            TorrentTracker.createTorrentFile(Paths.get(SaveFolder, argument.get(input)).toString(), Paths.get(SaveFolder, "torrent.torrent").toString(), intrfc);

            TorrentTracker trackerServer = new TorrentTracker(Paths.get(SaveFolder, "TorrentFile", "torrent.torrent").toString(), intrfc);
            trackerServer.startAnnouncement();
            TorrentClient torrentClient = new TorrentClient(intrfc.getHostAddress(), Paths.get(SaveFolder).toFile(), Paths.get(SaveFolder, "TorrentFile", "torrent.torrent").toFile(), intrfc);
            torrentClient.run();

        } else if (argument.contains("-transClient")){
            List<InetAddress> ifaces = TorrentClient.getInetInterface();

            for (InetAddress iface: ifaces) {
                n = ifaces.indexOf(iface);
                System.out.println(n + ")\t" + iface.getCanonicalHostName());
            }
            n = in.nextInt();
            intrfc = ifaces.get(n);
            String ip = null;
            MulticastClient.run();
            do {
                ip = MulticastClient.getIp();
            } while (ip == null);
            try { Thread.sleep(5000); } catch (InterruptedException e) {e.printStackTrace();}

            GetTorrent GT = new GetTorrent(Paths.get(SaveFolder, "TorrentFile").toFile(), ip);
            GT.run();

            TorrentClient torrentClient = new TorrentClient(ip, Paths.get(SaveFolder).toFile(), Paths.get(SaveFolder, "TorrentFile", "torrent.torrent").toFile(), intrfc);
            torrentClient.run();
        }else {
            System.out.println("Invalid Command");
        }
    }
}