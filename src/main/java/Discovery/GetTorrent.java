package Discovery;

import java.io.*;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Path;
import java.nio.file.Paths;

public class GetTorrent{
    private final File SaveFolder;
    private final String ServIP;
    private final String saveName;
    private final int port;


    public GetTorrent(File saveFolder, String servIP, int port, String saveName) {
        SaveFolder = saveFolder;
        ServIP = servIP;
        this.saveName = saveName;
        this.port = port;
    }

    public GetTorrent(File SaveFolder, String ServIP) {
        this.SaveFolder = SaveFolder;
        this.ServIP = ServIP;
        this.saveName = "torrent.torrent";
        this.port = 4559;
    }

    public void run() {
        try {
//            System.out.println("IP: " + this.ServIP);

            String URL = String.format("http://%1$s:%2$s/%3$s", this.ServIP, this.port, this.saveName);

//            System.out.println(URL);

            URL website = new URL(URL);
            ReadableByteChannel rbc = Channels.newChannel(website.openStream());
//            System.out.println(this.SaveFolder.getAbsolutePath() + "---" + this.saveName);


            Path paths = Paths.get(this.SaveFolder.toString());
            if (!paths.toFile().exists()){ paths.toFile().mkdirs();
//                System.out.println("mkdir");
//                System.out.println(paths.toString());
            }

//            System.out.println(paths);

            FileOutputStream fos = new FileOutputStream(Paths.get(this.SaveFolder.getAbsolutePath(), this.saveName).toFile().toString(), true);

            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            Thread.currentThread().interrupt();
        }
    }

//    public static void main(String[] args) {
//        GetTorrent g = new GetTorrent(new File("/Users/panteparak/Desktop/Test"), "1.bp.blogspot.com",  80, "/-l1r_ctHKtig/VP-8yjjz38I/AAAAAAAALSE/mIrHOlrZw-U/s1600/Cat.jpg");
//        new Thread(g).run();
//    }
}

