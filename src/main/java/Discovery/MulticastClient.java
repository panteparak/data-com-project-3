package Discovery;

import java.io.*;
import java.net.*;

public class MulticastClient {

    public static String getIp() {
        return ip;
    }

    public static String ip = null;

    public static void run() throws IOException {

        boolean boundSocket = false;
        boolean gotConnection = false;
        InetAddress address = InetAddress.getByName("0.0.0.0");
        DatagramSocket socket = null;
        while (!boundSocket){
            boundSocket = true;
            try {
                socket = new DatagramSocket(4556, address);
            } catch (BindException e) {
                boundSocket = false;
            }
        }
        //socket.bind(new InetSocketAddress(address, 4556));
        DatagramPacket packet;

        byte[] buf = new byte[256];
        packet = new DatagramPacket(buf, buf.length);
        System.out.println("Waiting");
        socket.receive(packet);

        String IP = new String(packet.getData(), 0, packet.getLength());
        System.out.println("Now: " + IP);
        ip = IP;

        socket.close();

        Socket sock;
        PrintWriter out;
        BufferedReader in;

        while (!gotConnection) {
            try {
                sock = new Socket(IP, 4557);
                out = new PrintWriter(sock.getOutputStream(), true);
                in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
                out.println("Got Connection");
                out.println(sock.getInetAddress());
                System.out.println(in.readLine());
                System.out.println(in.readLine());
                System.out.println("done");
                out.close();
                in.close();
                sock.close();
                gotConnection = true;
            } catch (IOException e) {
                gotConnection = false;
            }
        }

    }



}