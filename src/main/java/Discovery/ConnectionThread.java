package Discovery;

import java.io.*;
import java.net.*;
import java.util.*;

public class ConnectionThread extends Thread {
    protected ServerSocket serverSocket = null;
    protected PrintWriter out = null;
    protected BufferedReader in = null;
    private long FIVE_SECONDS = 5000;

    public ConnectionThread() throws IOException {
        this("ConnectionThread");
    }

    public ConnectionThread(String name) throws IOException {
        super(name);
    }

    public void run() {
        do {
            try {
                serverSocket = new ServerSocket(4557);
                System.out.println("awaiting connection");
                Socket clientSocket = serverSocket.accept();
                System.out.println("accepted");
                out = new PrintWriter(clientSocket.getOutputStream(), true);
                in = new BufferedReader(
                        new InputStreamReader(clientSocket.getInputStream()));
                String recvd = in.readLine();
                System.out.println(recvd);
                recvd = in.readLine();
                System.out.println(recvd + " -- connected to -- " + clientSocket.getInetAddress().getHostAddress());
                out.println(clientSocket.getInetAddress());
                out.println("Ok");
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (true);

    }
}
