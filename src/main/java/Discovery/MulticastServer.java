package Discovery;

import static java.lang.Thread.sleep;

public class MulticastServer {
    public static void main(String[] args) throws java.io.IOException {
        //new MulticastThread().start();
        new ConnectionThread().start();

        int count = ConnectionThread.activeCount();
        while (true) {
            if (ConnectionThread.activeCount() != count) new ConnectionThread().start();
            try {sleep(5000);} catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}