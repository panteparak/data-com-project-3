package Discovery;

import java.io.*;
import java.net.*;
import java.util.*;

public class MulticastThread extends Thread {
    protected DatagramSocket socket = null;
    protected boolean more = true;
    protected NetworkInterface current = null;
    protected Enumeration<NetworkInterface> interfaceList = null;
    protected InetAddress inetAddress = null;
    private long FIVE_SECONDS = 5000;

    public MulticastThread(InetAddress inetAddress) throws IOException {
        this("MulticastServerThread", inetAddress);
    }

    public MulticastThread(String name, InetAddress inetAddress) throws IOException {
        super(name);
        this.inetAddress = inetAddress;
        System.out.println("-------------Starting broadcast thread-------------");
        socket = new DatagramSocket(4555);
        socket.setBroadcast(true);
    }

    public void run() {
        while (more) {
            try {
                if (interfaceList != null && interfaceList.hasMoreElements()) {
                    current = interfaceList.nextElement();
                }
                else {
                    try {
                        interfaceList = NetworkInterface.getNetworkInterfaces();
                    } catch (SocketException e) {continue;}
                    current = interfaceList.nextElement();
                }
                byte[] buf;

                List<InterfaceAddress> localHostList = current.getInterfaceAddresses();
                if (localHostList.size() == 0) continue;
                InterfaceAddress localhost = localHostList.get(0);
                if (localhost.getAddress().getHostAddress().length() > 15) continue;
                if (!Objects.equals(localhost.getAddress().getHostAddress(), inetAddress.getHostAddress())) continue;
                String dString = localhost.getAddress().getHostAddress();
                buf = dString.getBytes();

                // send it
                if (localhost.getBroadcast() == null) continue;
                InetAddress broadcastAddr = InetAddress.getByName(localhost.getBroadcast().getHostAddress());
                DatagramPacket packet = new DatagramPacket(buf, buf.length, broadcastAddr, 4556);
                socket.send(packet);
                try {
                    sleep((long)(Math.random() * FIVE_SECONDS));
                } catch (InterruptedException e) { e.printStackTrace(); }
            } catch (IOException e) {
                e.printStackTrace();
                more = false;
            }
        }
        socket.close();
    }
}
