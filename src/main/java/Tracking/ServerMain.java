package Tracking;

import java.io.File;
import static spark.Spark.*;
    /*
     * TODO: Add route /
     * index page with Ajax script and Auto update
     */

    /*
     * TODO: Add route /api/tracking/update
     * POST Method: Add Data to server
     * GET Method: Retrieve latest data from server (JSON)
     *
     */

public class ServerMain {
    static class Config {
        static final int DEFAULT_PORT = 4559;
    }


    public static void startWebServer(File torrentFile){
        port(Config.DEFAULT_PORT);
        externalStaticFileLocation(torrentFile.getPath());
        staticFileLocation("/web/public");
        DataModel model = new DataModel();

        post("/api/tracking/update", (req, res) -> {
            String ip = req.headers("IPAddr");
            String progressStr = req.headers("progress");
            String state = req.headers("currentState");
            Float progress = null;

            try{
                progress = Float.parseFloat(progressStr);
            }catch (Exception e){
                halt(400, "Progress Exception");
            }

            if ((ip != null && ip.length() > 0) && (state != null && state.length() > 0)){
                model.addData(ip, progress, state);
                res.status(200);
            } else {
                if (ip == null || ip.length() == 0){
                    halt(400, "IP Exception");
                } else {
                    halt(400, "currentState Exception");
                }
            }
            return "OK";
        });

        get("/api/tracking/update", (req, res) -> model.getJSONProgress());

        after((req, res) -> res.header("Access-Control-Allow-Origin", "*"));
    }

    public static void main(String[] args) {
        startWebServer(new File("/Users/panteparak/Desktop"));
    }
}
