package Tracking;

import com.google.gson.Gson;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class DataModel {
    private final Map<String, TrackingData> storage;

    public DataModel() {
        this.storage = new ConcurrentHashMap<>();
    }

    public void addData(String ip, float percentage, String state){
        this.storage.compute(ip, (k, v) -> v == null || v.percentage <= percentage ? new TrackingData(ip, percentage, state) : v);
    }

    public Map<String, TrackingData> getProgress(){
        return this.storage;
    }

    public String getJSONProgress(){
        return new Gson().toJson(this.storage);
    }

    static class TrackingData implements Comparable<TrackingData>{
        public final String ip;
        public final float percentage;
        public final String state;

        public TrackingData(String ip, float percentage, String state) {
            this.ip = ip;
            this.percentage = percentage;
            this.state = state;
        }
        @Override
        public int compareTo(TrackingData that) {
            return Float.compare(this.percentage, that.percentage);
        }
    }
}