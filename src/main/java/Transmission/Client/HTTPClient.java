package Transmission.Client;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

public class HTTPClient implements Callable<HttpResponse> {
    private final String IPAddr;
    private final float Progress;
    private final String currentState;
    private final String URLServer;

    public HTTPClient(String URLServer,String IP, float progress, String currentState) {
        this.IPAddr = IP;
        this.Progress = progress;
        this.currentState = currentState;
        this.URLServer = URLServer;
    }

    @Override
    public HttpResponse call() throws Exception {
        CloseableHttpAsyncClient httpclient = HttpAsyncClients.createDefault();
        try {
            httpclient.start();
            HttpPost request = new HttpPost(this.URLServer);
            request.addHeader("IPAddr", this.IPAddr);
            request.addHeader("progress", Float.toString(this.Progress));
            request.addHeader("currentState", this.currentState);
            Future<HttpResponse> future = httpclient.execute(request, null);
            return future.get();
        } finally {
            httpclient.close();
        }
    }
}
