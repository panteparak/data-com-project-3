package Transmission.Client;

import com.turn.ttorrent.client.Client;
import com.turn.ttorrent.client.Client.ClientState;
import com.turn.ttorrent.client.SharedTorrent;

import java.io.File;
import java.io.IOException;
import java.net.*;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
public class TorrentClient{
    private final File saveLocation;
    private final File torrentLocation;
    public Client client;
    public static InetAddress ip;
    public static ExecutorService threadPool;
    public static String serverIP;
    public static boolean complete;

    public TorrentClient(String serverIP, File saveLocation, File torrentLocation, InetAddress ip) {
        this.saveLocation = saveLocation;
        this.torrentLocation = torrentLocation;
        this.ip = ip;
        this.threadPool = Executors.newFixedThreadPool(4);
        this.serverIP = serverIP;
        this.complete = false;
//        System.out.println(ip.getHostAddress());
    }

    public void run(){
        try {
//            System.out.println("Broadcasting Address: " + InetAddress.getLocalHost().getHostAddress());
            System.out.println("Broadcasting Address: " + this.ip.getHostAddress());

            String UpdateURL = String.format("http://%1$s:%2$s/api/tracking/update", this.serverIP, 4559);
            this.client = new Client(this.ip, SharedTorrent.fromFile(this.torrentLocation, this.saveLocation));
            this.client.setMaxUploadRate(0.0);
            this.client.setMaxDownloadRate(0.0);

//            final InetAddress iip = this.ip;

//            FIXME: Observer is Syncronized, Will block if interrupted
            this.client.addObserver((o, arg) -> {
                Client client = (Client) o;
                float progress = client.getTorrent().getCompletion();
                int deltaPercent = 1;
                int MaxSecondsToUpdate = 2;

                if (Math.round(progress) % 2 == 0 && !complete) {
                    StringBuilder bar = new StringBuilder();
                    for (int i = 0; i < progress / 2; i++) {
                        bar.append('*');
                    }
                    for (int i = 0; i < 50 - (progress / 2); i++) {
                        bar.append('-');
                    }
                    System.out.printf("\rProgress |%s| - %.2f%s", bar.toString(), progress,"%");
                }
                if (progress == 100.00 && !complete) {
                    complete = true;
                    StringBuilder bar = new StringBuilder();
                    for (int i = 0; i < progress / 2; i++) {
                        bar.append('*');
                    }
                    for (int i = 0; i < 50 - (progress / 2); i++) {
                        bar.append('-');
                    }
                    System.out.printf("\rComplete & Seeding |%s| - %.2f%s\n", bar.toString(), progress,"%");
                }

                boolean timeLimit = Math.abs(Instant.now().getEpochSecond() - TorrentInfo.lastUpdate) >= MaxSecondsToUpdate;
                boolean deltaProgressLimit = Math.abs(progress - TorrentInfo.progress) >= deltaPercent;
                boolean stateChange = TorrentInfo.lastState == null || !TorrentInfo.lastState.equals(client.getState());

                if (deltaProgressLimit || timeLimit || stateChange){
                    TorrentInfo.progress = (int) progress;
                    TorrentInfo.lastUpdate = Instant.now().getEpochSecond();
                    TorrentInfo.lastState = client.getState();

                    TorrentClient.threadPool.submit(new HTTPClient(UpdateURL, TorrentClient.ip.getHostAddress(), progress, client.getState().toString()));
                }
            });
            this.client.share();
            this.client.waitForCompletion();
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (NoSuchAlgorithmException e2) {
            e2.printStackTrace();
        }
    }

    public static List<InetAddress> getInetInterface(){
        NetworkInterface current;
        List<InetAddress> ifaceList = new ArrayList<>();

        try {
//            Wrote by Peter S.
//            Modified by Pan T.
            Enumeration<NetworkInterface> interfaceList = NetworkInterface.getNetworkInterfaces();

            while (interfaceList.hasMoreElements()){
                current = interfaceList.nextElement();
//                Null Pointer Exception
                List<InterfaceAddress> localHostList = current.getInterfaceAddresses();
                if (localHostList.size() == 0) continue;
                InterfaceAddress localhost = localHostList.get(0);

                if (localhost.getAddress().getHostAddress().length() > 15) continue;

                ifaceList.add(localhost.getAddress());
            }
        } catch (SocketException e){
            e.printStackTrace();
        }
        return ifaceList;
    }

    static class TorrentInfo{
        public static int progress = 0;
        public static long lastUpdate = 0;
        public static ClientState lastState = null;
    }
}
