package Transmission.Server;

import com.turn.ttorrent.common.Torrent;
import com.turn.ttorrent.tracker.TrackedTorrent;
import com.turn.ttorrent.tracker.Tracker;
import java.io.*;
import java.net.*;
import java.nio.file.*;
import java.security.NoSuchAlgorithmException;
import java.util.*;


public class TorrentTracker {

    static class Config{
        static final int DEFAULT_PORT = 6969;
        static final int DEFAULT_PIECE_LENGTH = 1024 * 1024;
    }
    private final Tracker tracker;
    private final int port;
    private final File saveTorrentLocation;
    private final InetAddress iface;

    public TorrentTracker(String torrentLocation, InetAddress iface) throws IOException{
        this(Config.DEFAULT_PORT,torrentLocation, iface);
    }

    public TorrentTracker(int port, String torrentLocation, InetAddress iface) throws IOException {
//        if (!torrentLocation.endsWith(".torrent")) throw new Exception("Invalid torrent file extension");
        this.tracker = new Tracker(new InetSocketAddress(iface, port));
        this.tracker.start();
        this.port = port;
        this.saveTorrentLocation = new File(torrentLocation);
        this.iface = iface;
    }

    public String buildMagnetLink(String displayName){
        return "";
    }


    public static void createTorrentFile(String fileLocation, String saveTorrentLocation, InetAddress inetAddress) {
        createTorrentFile(fileLocation, saveTorrentLocation, Config.DEFAULT_PORT, inetAddress);
    }

    public static void createTorrentFile(String inputFile, String torrentFile, int port, InetAddress inetAddress) {

        try {
            String url = TorrentTracker.class.getProtectionDomain().getCodeSource().getLocation().getFile();
            String decode = URLDecoder.decode(url, "UTF-8");
            String SaveFolder = new File(decode).getParentFile().getPath();
            File fileLocation = new File(inputFile);

            Path paths = Paths.get(SaveFolder, "TorrentFile");
            if (!paths.toFile().exists()){ paths.toFile().mkdir();
//                System.out.println("mkdir");
                System.out.println(paths.toString());
            }

            File saveTorrentLocation = paths.toFile();
            Path torrentFilePath = Paths.get(paths.toString(), "torrent.torrent");
            List<List<URI>> uris = new ArrayList<>(Arrays.asList(getTrackerURL(port, inetAddress)));
            Torrent torrent = Torrent.create(fileLocation,Config.DEFAULT_PIECE_LENGTH, uris, "PeterPan");
//            System.out.println(saveTorrentLocation.getAbsoluteFile());
            torrent.save(new FileOutputStream( torrentFilePath.toFile()));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e){
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e){
            e.printStackTrace();
        }
    }

    public void startAnnouncement() {
        try {
            this.tracker.announce(TrackedTorrent.load(this.saveTorrentLocation));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e){
            e.printStackTrace();
        }
    }

    public static List<URI> getTrackerURL(int port, InetAddress inetAddress) {

//        TODO: Fix This SHIT!!!!
        NetworkInterface current;
        List<URI> ifaceList = new ArrayList<>();

//        try {
//            Enumeration<NetworkInterface> interfaceList = NetworkInterface.getNetworkInterfaces();
//            while (interfaceList.hasMoreElements()){
//                current = interfaceList.nextElement();
//                List<InterfaceAddress> localHostList = current.getInterfaceAddresses();
//                if (localHostList.size() == 0) continue;
//
//                InterfaceAddress localhost = localHostList.get(0);
//                if (localhost.getAddress().getHostAddress().length() > 15) {
//                    continue;
//                }
//                if (Objects.equals(localhost.getAddress().getHostAddress(), inetAddress.getHostAddress())) {
//                    ifaceList.add(new URI(String.format("http://%1$s:%2$s/announce", localhost.getAddress().getHostAddress(), port)));
////                    System.out.println("Found IFaceIP: " + localhost.getAddress().getHostAddress());
//                }
//            }
//        } catch (SocketException e){
//            e.printStackTrace();
//        } catch (URISyntaxException e){
//            e.printStackTrace();
//        }
//        System.out.println(ifaceList);

        try {
            ifaceList.add(new URI(String.format("http://%1$s:%2$s/announce", inetAddress.getHostAddress(), port)));

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
//        ifaceList.add(new URI(String.format("http://%1$s:%2$s/announce", inetAddress.getHostAddress(), port)));
        return ifaceList;
    }
}
