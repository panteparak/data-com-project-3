package Transmission.Server.ServerTest;

import java.net.*;
import java.nio.channels.UnsupportedAddressTypeException;
import java.util.Enumeration;

/**
 * Created by panteparak on 11/24/16.
 */
public class INET {
    public static void main(String[] args) throws Exception{
//        System.out.println(Inet4Address.getLocalHost().getHostAddress());
        System.out.println(getIPv4Address("en1"));
    }

    private static Inet4Address getIPv4Address(String iface)
            throws SocketException, UnsupportedAddressTypeException,
            UnknownHostException {
        if (iface != null) {
            Enumeration<InetAddress> addresses = NetworkInterface.getByName(iface).getInetAddresses();
            while (addresses.hasMoreElements()) {
                InetAddress addr = addresses.nextElement();
                if (addr instanceof Inet4Address) {
                    return (Inet4Address)addr;
                }
            }
        }

        InetAddress localhost = InetAddress.getLocalHost();
        if (localhost instanceof Inet4Address) {
            return (Inet4Address)localhost;
        }

        throw new UnsupportedAddressTypeException();
    }
}
