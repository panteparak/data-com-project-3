var ipToID = (IP) => IP.replace(/\./g,'-');
var inValID;

$(document).ready(() => {
  main();
  inValID = setInterval("main()", 1000);
});

var fetchData = () => {
  return new Promise((accept, reject) => {
    var XHR = new XMLHttpRequest();
    XHR.open("GET", "/api/tracking/update", true);
    XHR.send();

    XHR.onload = () => {
      if (XHR.status == 200) accept(JSON.parse(XHR.responseText));
      else reject("Failed to fetch Data, Code:", XHR.status);
    }

    XHR.onerror = () => reject("Failed to fetch Data, Code: " + XHR.status);
  })
}

var renderHTML = (data) => {
  async.each(data, (v, cb) => {
    var ip = v["ip"];
    var state = v["state"];
    var progress = v["percentage"];


    if ($(".container #" + ipToID(ip)).length == 0) {
      $(".container").append("<div id='" + ipToID(ip) + "'></div>");
      $(".container #" + ipToID(ip)).append("<h3 id='ip'>IP: </h3>");
      $(".container #" + ipToID(ip)).append("<h3 id='IPAddr'>" + ip + "</h3>");
      $(".container #" + ipToID(ip)).append("<h3> | State: </h3>");
      $(".container #" + ipToID(ip)).append("<h3 id='torrentState'>" + state + "</h3>")
      $(".container #" + ipToID(ip)).append("<div class='progress'></div>")
      $(".container #" + ipToID(ip)+ " .progress").append("<div class='progress-bar progress-bar-info progress-bar-striped active' role='progressbar' aria-valuemin='0' aria-valuemax='100'></div>");
    }
    $(".container div#" + ipToID(ip) + " h3#torrentState").html(state);
    $("div.container div#" + ipToID(ip) + " .progress .progress-bar.progress-bar-info.progress-bar-striped.active").attr("aria-valuenow", progress);
    $("div.container div#" + ipToID(ip) + " .progress .progress-bar.progress-bar-info.progress-bar-striped.active").attr("style", "width:" + progress + "%");
    $("div.container div#" + ipToID(ip) + " .progress .progress-bar.progress-bar-info.progress-bar-striped.active").text(v["percentage"] == "0.0" ? "" : v["percentage"] + "%");

    if(progress == "100.0")
      $("div.container div#" + ipToID(ip) + " .progress .progress-bar.progress-bar-info.progress-bar-striped.active").attr("class", "progress-bar progress-bar-success");

    cb();
  }, (err) => {
    if (err) console.log(err);
  });
}

var main = () => {
  fetchData()
  .then((incoming) => renderHTML(incoming))
  .catch((err) => {
    console.log(err);
    // clearInterval(inValID);
  });
}
