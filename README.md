# Welcome to ICCS 223 Data Communication and Network: Project 3
## Large File Transfer

Phases:
------
* Discovery (Java)
* Transmission (Java)
* Tracking (Java + HTML + Javascript)
* * *

Languages:
------
* Java 1.8
* Javascript
* * *

Dependencies:
------
* Apache HTTP Async Client
* Apache Spark
* TTorrent
* Ajax 
* jQuery
* * *

TO RUN:
------
* Client 'java -jar PeterPanTransferService.jar -transClient'
* Server 'java -jar PeterPanTransferSercive.jar -transServer -i <file>'
* * *

Distribution Progress Tracking:
------
* http://<ServerIP>:4559/hello
* http://<ServerIP>:4559/ (Experimentals) 
* * *

Contributors:
------
* Pan TEPARAK (Transmission/Tracking)
* Peter Son STRUSCHKA (Discovery/Merge Fixer)